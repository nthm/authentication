# Refreshing/Maintaining authentication on other pages

The authentication portal has a way of getting tokens and CloudFront cookies for
a user so they can move to a new service. After login, the portal passes the
user to a service with all their tokens (ID/Access/Session/Refresh) as cookies.
They're allowed into the service by CloudFront because they have signed cookies,
unrelated to the tokens. The tokens are passed as cookies only because
LocalStorage isn't accessible across subdomains. Both the tokens and all cookies
will eventually expire if not refreshed, and CloudFront will deny them access
and send them back to the portal. Once there, they'd refresh, and then be send
back to where they were. This might happen ever hour or so. It works, but has a
bad user experience.

To avoid that, there needs to be a module that can be given to each service as a
way of keeping a session alive. This code needs to refresh both the tokens and
cookies to keep the user validated.

There's no backend API to the portal that the user can call to refresh tokens
and cookies; the authentication portal is serverless. Instead, everything
happens on the client through Cognito (user management and authentication as a
service) and Lambdas (servers as a service). The authentication portal uses
_amazon-cognito-identity-js_, which is part of _Amplify_, for tokens. CloudFront
cookies are genereated in a Lambda, which is invoked using the AWS SDK for JS.

Note that refreshing tokens on a service will replaces the tokens _of that
service_. Tokens that are stored in the LocalStorage of authentication.tut will
_not_ be updated.

## Authentication libraries on the portal

Two seperate methods were needed to acquire tokens and cookies on the portal.

### Tokens

Acquiring and refreshing tokens was done through _amazon-cognito-identity-js_,
since Cognito uses a very special non-standard authentication flow that requires
an eloborate dance of JSON requests and cryptography to work out.

### CloudFront cookies

For cookies, the user needs to call a Lambda. In the portal this was done via
AWS.Lambda(), part of _aws-sdk_, because it wasn't clear how to create a network
request to call a Lambda - requests need to be signed using AWS Signature v4,
which looked complicated by all the modules pulled into the bundle. To sign a
request, the user would need AWS credentials (different from Cognito
credentials) which were acquired by displaying their ID token into the SDK and
it making a network request.

The code for that way of refreshing tokens is in _docs/snippets/_.

## Copying libaries to each service

While the Cognito library was installed on the portal, it wouldn't be on any
service; each would need to download the 140kb (~14kb gzipped) to run.

Even then, reviving a Cognito session won't be enough since the user will still
be thrown back to the portal by CloudFront when their signed cookies expire.
Defeating the purpose of refreshing on the service.

To request cookies from the Lambda, each service would need parts of the AWS
SDK. This minimum SDK is 680kb (~70kb gzipped) even when selectively importing
only the necessary parts for the Lambda call. Giving them the benefit of the
doubt - while it looks like NIH syndrome by rewriting everything from XHRs to
event emitters and MD5, it could be that browser support wasn't there at the
time they were implemented. Regardless, it's large.

## Alternatives

After some research, it turns out Cognito supplies an OAuth2 TOKEN endpoint that
accepts a simple POST request to refresh tokens. This is the same enpoint I used
to perform federated login (sign in with Google). A similar endpoint doesn't
exist for the initial authentication, but it means services won't need to pull
in the library for refreshing tokens.

Looking at the network logs for the Lambda invocation, the and reading lots on
signed URLs and headers, it seems the call only needs to be signed using the AWS
Signature V4. Two npm modules exist, `aws4` and `aws-v4-sign-small`. Reading
their code shows that the signing process is mostly HMACs and SHA256 hashing.
Being able to swap the entire AWS SDK for one of these would be ideal.

The first library works (!) but after looking at the bundle analysis it's clear
it imports Node's `crypto` library, which towers in size over the AWS SDK at
785kb (~115kb gzipped)!

The second library is a fork of the other that aims to be smaller and target
browsers by using a slimmer crypto library, CryptoJS. It and all of its
dependencies weigh in at around ~90kb (~10kb gzipped). However, it doesn't work.
It almost does, but the signature is wrong, and it's clear in the code there are
typos in variable names that are at fault. I tested CryptoJS, it's fine (but it
doesn't use Uint8Array which is odd). The library was just not written well.
Turns out it has been unmaintained for years.

Speaking of, `aws4` is more than 5 years old! This explians the legacy code in
the AWS SDK too. After debugging the smaller alternative and getting it to work
it's clear that the only crypto functions needed are HMAC-SHA256 and SHA256 for
hashing.

## Today

You might think, can't browsers do HMAC-SHA256 and SHA256? Yes they can! The
WebCrypto API has great support in this domain, and only iOS might need a
polyfill (async import for CryptoJS).

I wrote a smaller implementation of an AWS Signature v4 signer with that in
mind. It targets only Lambdas, but can easily be extended to have feature parity
with other libaries. It uses the native WebCrypto to be as light as possible.

The result was a set of functions, around 6kb raw (unknown for minified and
gzipped, sorry).

You can:
  - request a token refresh, by showing your refresh token
  - request CloudFront cookies. internally, this requests AWS credentials if
    needed and then generates signed request headers.

The script `refresher.js` is a drop in module that handles refreshing both
tokens and cookies automatically. It will start a timer for each credential that
renews by looking at the next expiry times as they come in.

It works via side effects:

```js
import './path/to/refresher'
```

## Future work

Could import this to a service worker, which could handle failed network
requests by automatically refreshing tokens and then trying again.

If polyfills are needed for iOS devices, promisify the following CryptoJS
libaries and have them plug into the `Crypto` object. It was written that way
for this purpose, but removed in the end. You'll also need their _core.min.js_.

https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.9-1/hmac-sha256.min.js
https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.9-1/sha256.min.js
