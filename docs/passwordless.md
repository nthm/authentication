# Passwordless authentication

There seem to be 4 ways to do it in the field. All but one are email-based,
which is what we'll talk about.

The other one isn't an option, which is sending a code to their phone via SMS
and having the browser either wait for them to enter it, or allow an option to
confirm a code from a new session. We don't want to spend money sending texts -
texting is a dead system anyway.

## Options

1. Generate a random token and email them a link `site.ca/magiclink/${token}`
   and the token is long, complicated, and hard to brute force.

1. Send the authentication request to them as a link. Literally the request they
   need to sign in - exactly the ones they'd get if they signed in with a
   password or federated login (Google, LinkedIn). This is what Auth0 does.

3. Sign a payload (anything) with the Cognito private key, and send that.
   Because it's a private/public key pair it's easy to test validity and you
   don't need a database to store tokens.

## Issues:

2# is bad because the only way to have link expiry is to have the tokens expiry
just as fast. It's nice to have an extra step to touch base with the server to
confirm everything. Also, the Cognito flow doesn't really support handing out
tokens at any time. The process is kind of setup to always return the tokens to
the browser, not email them off. So you'd need to do some hacks like "failing"
the login even though it's valid... Not ideal.

Similarly, 3# isn't something Cognito can easily do. Its private keys are, for
good reason, hidden from Lambda functions, so we'd need a seperate KMS key for
that... 3# only really shines through when you want to avoid a database _but_
Cognito's Lambda triggers are able to share state (!) which removes that issue.

Going with 1#. Generating long tokens.

## Examples from around the web:

AWS re:Invent 2016 Cognito demos showing off SMS-based passwordless by
generating 7 digit codes in a Lambda using `Math.random().toString(10).substr`
https://youtu.be/8DDIxqIW1sM?t=54m26s

Auth0 just emails the authentication request as a link, by basically preloading
it with a username/email and "verification code" (i.e password). See their
screenshot for details:
https://auth0.com/docs/connections/passwordless/spa-email-link

These are together:
https://medium.com/one-more-thing-studio/how-to-make-magic-links-with-node-1d164c036e29
https://github.com/jdrouet/magic-link-example/blob/master/source/service/authentication.js
