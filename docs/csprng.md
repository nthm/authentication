# Cryptographically secure random number generation

When writing _passwordless.md_ to described a few ways of generating tokens to
be emailed to a user for sign in. I also mentioned the way AWS generates 7 digit
long numeric pins token for SMS, using `Math.random().toString(10).substr`.

## `Math.random()`

If you search for `Math.random()` on the internet, you will be told over and
over again to not use the function for any remotely cryptographic tasks or
situations where the application requires on the output to be truely random.

This is because the pseudo-random number generator (PRNG) used in
`Math.random()` is not designed to be cryptographically secure. There are plenty
of incredibly detailed posts explaining why that is, and it's a fact agreed on
by the developers of the V8 engine as well. It's only designed to be fast.

ECMAScript only defines the function as a number that needs to pseudo randomly
return a 32 bit floating point number between 0 and 1. Implementation is left up
to each engine - see resources for a list.

For AWS, it's a simple solution fitting for a demo, and given that the code was
never published outside of the presentation, and that the AWS has not yet fully
supported/merged passwordless authentication (see below), it's also possible
they don't recommend `Math.random()` for production.

An open (as of May 25, 2018) pull request for passwordless in AWS Amplify:
https://github.com/aws/aws-amplify/pull/566/files

Here are some resources on `Math.random()`:

The original design decisions behind the algorithm:
https://security.stackexchange.com/questions/181580/why-is-math-random-not-designed-to-be-cryptographically-secure

An excellent breakdown of PRNGs in the ecosystem:
https://security.stackexchange.com/questions/84906/predicting-math-random-numbers

The above links to this very in depth article breaking down the algorithm's
issues:
https://medium.com/@betable/tifu-by-using-math-random-f1c308c4fd9d

V8's response to the above article, releasing a patched implementation deployed
in Chrome 49. They also confirm that both versions, the previous and the new
patch, are not cryptographically secure and should not be used for that case:
http://v8project.blogspot.fr/2015/12/theres-mathrandom-and-then-theres.html

An exploitation used to predict winnings for a CSGO bidding bot:
http://jonasnick.github.io/blog/2015/07/08/exploiting-csgojackpots-weak-rng/

## Alternatives:

NodeJS and browsers support cryptographically secure random number generation
methods through either the `crypto` library's `crypto.randomBytes` or the Web
Cryptography API's `window.crypto.getRandomValues`, respectively.

Note that these functions have relatively huge performance costs compared to
calling `Math.random()`

### URL safe strings:

```js
> crypto.randomBytes(64).toString('hex');
'903b8ad64d3b0951d597497bc08b14a7758258959d0abaded62ee5a13fc1b1a336df2f08d3515af35acc639ed921df9f92d3248cd492e9c1305cb7039004556d'.
```

### Replacement to `Math.random()` (i.e a number between 0 and 1):

```js
> crypto.randomBytes(4).readUIntLE(0, 4)
3233268525
> crypto.randomBytes(4).readUIntLE(0, 4) / 0xFFFFFFFF
0.5658767469147865
```

### Library

You can use the `secure-pin` library for NodeJS which is capable of generating
numbers, PINs, and strings for various character sets. Its readme shows a plot
of entropy and lots of research into collision resistance; it was able to
generate 2,700,000 strings of 10 characters without a collision, but then ran
out of memory.

All functions in the library including shuffling of arrays uses
`crypto.randomBytes` as its RNG.

The only possible note about the library is that the generations functions are
recursive.

https://github.com/ozouai/node-secure-pin/blob/master/index.js
