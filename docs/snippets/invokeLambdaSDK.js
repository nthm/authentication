// read the related documentation at docs/signing-lambda-requests.md

import AWS, { CognitoIdentityCredentials } from 'aws-sdk/global'
import Lambda from 'aws-sdk/clients/lambda'
import Cookies from 'js-cookie'
import {
  COGNITO_IDP_DOMAIN,
  CLOUDFRONT_COOKIES_LAMBDA_ARN,
  COOKIES_DOMAIN,
  AWS_REGION,
  FEDERATED_IDENTITY_POOL_ID,
  STORAGE
} from '../util/constants'
import { ls } from '../util'

const FetchSignedCookies = idToken => {
  // otherwise AWS thinks it's in Node and tries to use native stream APIs
  AWS.util.isNode = () => false
  AWS.util.isBrowser = () => true
  AWS.config.region = AWS_REGION

  const credentials = new CognitoIdentityCredentials({
    IdentityPoolId: FEDERATED_IDENTITY_POOL_ID,
    IdentityId: null,
    Logins: {
      [COGNITO_IDP_DOMAIN]: idToken,
    },
  })
  AWS.config.credentials = credentials
  return AWS.config.credentials.getPromise()
    .then(() => {
      console.log('credentials:', AWS.config.credentials)
      const lambda = new Lambda()
      const params = {
        FunctionName: CLOUDFRONT_COOKIES_LAMBDA_ARN,
        LogType: 'Tail', // return last 4KB of logs in a header for debugging
      }
      return lambda.invoke(params).promise()
    })
    .then(payload => {
      const cookies = JSON.parse(payload.Payload)
      console.log('received cookies:', cookies)
      const setRememberMe = ls.get(STORAGE.EMAIL)
      const daysCookiesValidFor = setRememberMe ? 2 : 1 // days

      const expiresOn = new Date()
      expiresOn.setDate(expiresOn.getDate() + daysCookiesValidFor)
      ls.set(STORAGE.EXPIRES_ON, expiresOn.toISOString())

      // rather than hard coding, just set all
      for (const [key, val] of Object.entries(cookies)) {
        Cookies.set(key, val, {
          domain: COOKIES_DOMAIN,
          expires: daysCookiesValidFor,
          secure: true,
        })
      }
    })
}

export { FetchSignedCookies }
