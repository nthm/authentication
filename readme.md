# Authentication

These are notes, documents, and parts of an authentication system developed at
Tutela in Spring 2018. It's build as a serverless Preact application that uses
AWS Cognito and Lambdas for a backend.

See docs/ for diagrams! (done in draw.io)

Not all code is uploaded here, sorry.

## Auth-related ingredients:

  - Cognito. A user directory/database for authentication and low-key
    authorization using AWS IAM roles. Supports some federation (sign in with
    Google) and supplies an OAuth2 server as well.

  - CloudFront. The border that keeps services and websites private, requiring
    users to have signed cookies to proceed.

  - Lambdas. Among many Cognito-initiated tasks such as passwordless login,
    these were also used to generate signed CloudFront cookies.

  - _amazon-cognito-identity-js_. For interacting with Cognito to login, sign
    up, and to request an email for either passwordless login or to reset
    passwords. The library has many more features, such as 2FA/TOTP, but weren't
    yet implemented.

  - WebCrypto API. Requests to Lambdas need to signed using the AWS Signature v4
    algorithm; a chain of HMAC-SHA256s and two SHA256 hashes. This API made
    it possible without any external dependencies.

  - Cookies and LocalStorage. The system worked by having the authentication
    portal and all services as subdomains under the same domain. This allows
    cookies to be sent between them, which is important for a serverless
    application.

I also wrote my work term report on the subject of authentication using AWS
Cognito, which is uploaded to this repository as well.

The project went through several phases (don't they all) which were motivated by
wanting to reduce the bundle size and code complexity. For example, the AWS SDK
was originally bundled for creating and signing Lambda invocations, which added
more than 700kb of dependencies. After learning how to write the signatures in
vanilla JS, I developed a module to sign the requests using the native WebCrypto
API - removing the SDK entirely. Bundle size always matters, but was espescially
important for that module as it was deployed to all private websites at Tutela.
It maintains a user's session once they've left the authentication portal. You
can read more about that in _docs/external-token-refresh_.
