import requestTokenRefresh from './requestTokenRefresh'
import requestAWSCredentials from './requestAWSCredentials'
import requestCloudFrontCookies from './requestCloudFrontCookies'
import Cookies from 'js-cookie'
import { COOKIE_DOMAIN, USERPOOL_CLIENT_ID } from '../../util/constants'

// helpers:

const base64ToJSON = data => JSON.parse(window.atob(data))

const lsAuthPrefix = `CognitoIdentityServiceProvider.${USERPOOL_CLIENT_ID}.`
const lsAuth = {
  get: (key) => window.localStorage.getItem(lsAuthPrefix + key),
  set: (key, val) => window.localStorage.setItem(lsAuthPrefix + key, val),
}

const expiryDelta = awsExpiryTimestamp => {
  // timestamps from AWS need to be converted to milliseconds
  const delta = new Date(Number(awsExpiryTimestamp + '000')) - new Date()
  return delta > 0 ? delta : 0
}

// assume this never changes for the duration of the session
const user = lsAuth.get('LastAuthUser')

// tokens:

const tokenLivetime = idToken => {
  if (!idToken) {
    // unlike cookies, don't proceed if there's no ID token
    throw new Error('No ID token')
  }
  // a JWT is <header>.<payload>.<signature> we want payload
  const idTokenPayload = base64ToJSON(idToken.split('.')[1])

  // payload has ~12 fields including email, exp (expiry), IAM role...
  const livetime = expiryDelta(idTokenPayload.exp)
  console.log('tokens expire in', livetime)
  return livetime
}

function refreshTokens() {
  const lastestRefreshToken = lsAuth.get(`${user}.refreshToken`)
  requestTokenRefresh(lastestRefreshToken)
    .then(({
      access_token, id_token, refresh_token,
    }) => {
      lsAuth.set(`${user}.idToken`, id_token)
      lsAuth.set(`${user}.accessToken`, access_token)
      lsAuth.set(`${user}.refreshToken`, refresh_token)

      // loop
      const newTokenLivetime = tokenLivetime(id_token)
      window.setTimeout(refreshTokens, newTokenLivetime)
    })
}

// cookies:

const cookieLivetime = policyCookie => {
  if (!policyCookie) {
    // should only happen on the portal or outside of COOKIE_DOMAIN subdomain
    console.log('no cookies')
    return 0
  }
  // there's always an underscore at the end that needs to be removed
  const cookiePolicy = base64ToJSON(policyCookie.slice(0, -1))
  const time = cookiePolicy.Statement[0].Condition.DateLessThan['AWS:EpochTime']

  const livetime = expiryDelta(time)
  console.log('cookies expire in', livetime)
  return livetime
}

function refreshCookies() {
  const latestIdToken = lsAuth.get(`${user}.idToken`)
  requestAWSCredentials(latestIdToken)
    .then(awsCreds => requestCloudFrontCookies(awsCreds))
    .then(cookies => {
    // rather than hard coding, just set all
      for (const [key, val] of Object.entries(cookies)) {
        Cookies.set(key, val, {
        // this will silently fail if it's not the current domain
          domain: COOKIE_DOMAIN,
          // expiry is arbitrary since it's hard set by CloudFront at 1 hour
          expires: 365,
          secure: true,
        })
      }

      // loop
      const newCookieLivetime = cookieLivetime(cookies['CloudFront-Policy'])
      window.setTimeout(refreshCookies, newCookieLivetime)
    })
}

// kick off the refresh intervals
const initialIdToken = lsAuth.get(`${user}.idToken`)
window.setTimeout(refreshTokens, tokenLivetime(initialIdToken))

const initialCookie = Cookies.get('CloudFront-Policy')
window.setTimeout(refreshCookies, cookieLivetime(initialCookie))
