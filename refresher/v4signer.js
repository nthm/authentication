// sign a request using the AWS Signature v4
// http://docs.amazonwebservices.com/general/latest/gr/signature-version-4.html

// written generally so it can be extracted for other services, but refer to the
// npm module, aws4, for nuances with some services like S3 and SES. also, I
// completely ignore query string (param) signing and polyfills for WebCrypto

// used for encoding the path before it's hashed as part of the signed headers
const encodeRFC3986 = urlEncodedString =>
  urlEncodedString.replace(/[!'()*]/g, c =>
    '%' + c.charCodeAt(0).toString(16).toUpperCase())

const Enc = new TextEncoder('utf-8')

const toHex = buffer => {
  const hexCodes = []
  const view = new DataView(buffer)
  const padding = '00000000'
  for (let i = 0; i < view.byteLength; i += 4) {
    const stringValue = view.getUint32(i).toString(16)
    hexCodes.push((padding + stringValue).slice(-padding.length))
  }
  return hexCodes.join('')
}

// note that SubtleCrypto is undefined in insecure contexts (not https)
const Crypto = {
  HmacSHA256(keyBuffer, dataBuffer) {
    const rawKeyBuffer =
        keyBuffer instanceof ArrayBuffer
          ? keyBuffer
          : Enc.encode(keyBuffer)

    return window.crypto.subtle.importKey('raw', rawKeyBuffer, { name: 'HMAC',
      hash: { name: 'SHA-256' } }, false, ['sign'])
      .then(key =>
        window.crypto.subtle.sign('HMAC', key, Enc.encode(dataBuffer)))
  },
  HashSHA256(str) {
    return window.crypto.subtle.digest('SHA-256', Enc.encode(str)).then(toHex)
  },
}

const generateSignatureHeaders = async ({ service, region, method, host, path,
  /* query, */ body }, awsCreds) => {

  // match the naming from the AWS credentials response payload
  const { AccessKeyId, SecretKey, SessionToken } = awsCreds

  const date = new Date()
  const amzDateTime = date.toISOString().replace(/[:-]|\.\d{3}/g, '')
  const amzDate = amzDateTime.substr(0, 8)

  const sigLabel = 'aws4_request'
  const preCredentialString = [amzDate, region, service, sigLabel].join('/')

  // TODO: these are only the headers needed for Lambdas
  // avoiding excessive .toLowerCase() calls by just having them be lowercase
  const headers = {
    host,
    'x-amz-security-token': SessionToken,
    'x-amz-date': amzDateTime,
  }

  const sortedHeaderKeys = Object.keys(headers).sort()
  const signedHeaders = sortedHeaderKeys.join(';')

  const canonicalHeaders =
    sortedHeaderKeys
      .map(key => `${key}:${headers[key]}`)
      .join('\n')

  // the path is unusually double encoded for signing
  const twiceEncodedPath = path.split('/').map(x =>
    encodeRFC3986(encodeURIComponent(encodeURIComponent((x))))).join('/')

  // TODO: ignored since it's not needed for Lambdas. refer to other modules
  const stringifiedQuery = ''

  const canonicalString = [
    method.toUpperCase(),
    twiceEncodedPath,
    stringifiedQuery,
    canonicalHeaders + '\n',
    signedHeaders,
    await Crypto.HashSHA256(body || ''),
  ].join('\n')

  const signaturePayload = [
    'AWS4-HMAC-SHA256',
    amzDateTime,
    preCredentialString,
    await Crypto.HashSHA256(canonicalString),
  ].join('\n')

  const kHeaderSecretDate = await Crypto.HmacSHA256('AWS4' + SecretKey, amzDate)
  const kRegion = await Crypto.HmacSHA256(kHeaderSecretDate, region)
  const kService = await Crypto.HmacSHA256(kRegion, service)
  const kSigLabel = await Crypto.HmacSHA256(kService, sigLabel)

  const signature = toHex(await Crypto.HmacSHA256(kSigLabel, signaturePayload))

  headers.Authorization = [
    `AWS4-HMAC-SHA256 Credential=${AccessKeyId}/${preCredentialString}`,
    `SignedHeaders=${signedHeaders}`,
    `Signature=${signature}`,
  ].join(', ')

  console.log(headers)
  return headers
}

export default generateSignatureHeaders
