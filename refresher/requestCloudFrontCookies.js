import generateSignatureHeaders from './v4signer'
import { CLOUDFRONT_COOKIES_LAMBDA_ARN } from '../../util/constants'

const SIGNV4_PAYLOAD = {
  service: 'lambda',
  region: 'us-west-2',
  method: 'POST',
  host: 'lambda.us-west-2.amazonaws.com',
  path: `/2015-03-31/functions/${CLOUDFRONT_COOKIES_LAMBDA_ARN}/invocations`,
}

// use AWS credentials to sign and invoke the Lambda
// pull out AccessKeyId, SecretKey, and SessionToken from awsCreds.Credentials

const encodedPath =
  SIGNV4_PAYLOAD.path.split('/').map(x => encodeURIComponent(x)).join('/')

const requestCloudFrontCookies = awsCreds =>
  generateSignatureHeaders(SIGNV4_PAYLOAD, awsCreds.Credentials)
    .then(headers =>
      fetch(`https://${SIGNV4_PAYLOAD.host}${encodedPath}`, {
        method: SIGNV4_PAYLOAD.method,
        headers,
      }))
    .then(res => res.json())
    .then(data => {
      console.log('Lambda returned:', data)
      return data
    })

export default requestCloudFrontCookies
