import {
  PUBLIC_URL as TOKEN_ORIGIN_URL,
  USERPOOL_URL,
  USERPOOL_CLIENT_ID
} from '../../util/constants'

const encodeURLParams = obj =>
  Object.entries(obj).map(x => x.map(encodeURIComponent).join('=')).join('&')

const requestTokenRefresh = cognitoRefreshToken =>
  fetch(`${USERPOOL_URL}/oauth2/token`, {
    method: 'POST',
    headers: {
      'content-type': 'application/x-www-form-urlencoded',
    },
    body: encodeURLParams({
      grant_type: 'refresh_token',
      client_id: USERPOOL_CLIENT_ID,
      redirect_uri: `${TOKEN_ORIGIN_URL}/idpresponse`,
      refresh_token: cognitoRefreshToken,
    }),
  })
    .then(res => res.json())
    .then(data => {
      console.log(data)
      return data
    })

export default requestTokenRefresh
