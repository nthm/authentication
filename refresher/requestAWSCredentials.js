import {
  COGNITO_IDENTITY_DOMAIN,
  FEDERATED_IDENTITY_POOL_ID,
  COGNITO_IDENTITY_URL
} from '../../util/constants'
import { ls } from '../../util'

const lsKeyForPoolIdMap = 'CognitoMappedIdentityId'

const cognitoRequest = (target, bodyJSON) =>
  fetch(COGNITO_IDENTITY_URL, {
    method: 'POST',
    headers: {
      'content-type': 'application/x-amz-json-1.1',
      'x-amz-target': `AWSCognitoIdentityService.${target}`,
    },
    body: JSON.stringify(bodyJSON),
  })
    .then(res => res.json())

const requestAWSCredentials = cognitoIdToken =>
  new Promise(resolve => {
    let cachedIdentityId = ls.get(lsKeyForPoolIdMap)
    return resolve(
      cachedIdentityId ||
      cognitoRequest('GetId', {
        IdentityPoolId: FEDERATED_IDENTITY_POOL_ID,
        Logins: { [COGNITO_IDENTITY_DOMAIN]: cognitoIdToken },
      })
        .then(({ IdentityId }) => {
          ls.set(lsKeyForPoolIdMap, IdentityId)
          return IdentityId
        }))
  })
    .then(identityId =>
      cognitoRequest('GetCredentialsForIdentity', {
        IdentityId: identityId,
        Logins: { [COGNITO_IDENTITY_DOMAIN]: cognitoIdToken },
      }))
    .then(data => {
      console.log(data)
      return data
    })

export default requestAWSCredentials
