const AWS = require('aws-sdk')
const crypto = require('crypto')

/* Everyone gets the same policy allowing access to POLICY_RESOURCE. If you want
 * more control, move the object to another bucket requiring authorization
 * methods from S3 (bucket policies and signed URLs)
 */

const POLICY_RESOURCE = 'https://*.site.com/*'
const ACCESS_DURATION = 86400 // 1 day

const KMS_KEY_PAIR_ID = '286444...cb3723'
const CLOUDFRONT_KEY_PAIR_ID = 'APKAI...6RJRQ'

// this is a very long key
const KMS_ENCRYPTED_CLOUDFRONT_PRIVATE_KEY = `
AQIvbDAH\Jsk...jK6UWKEhMcoAF/1VsXTmM=...
YHW782bS13dS...Yhwjdwoa33dlKef19pL13A...
`.split('\n').join()

exports.handler = async (event, context, callback) => {
  const kms = new AWS.KMS()
  const privateKey = await kms.decrypt({
    CiphertextBlob: new Buffer(KMS_ENCRYPTED_CLOUDFRONT_PRIVATE_KEY, 'base64'),
  }).promise()
    .then(data => data.Plaintext.toString('ascii'))
    .catch(callback)

  const policy = {
    Statement: [{
      Resource: POLICY_RESOURCE,
      Condition: {
        DateLessThan: {
          'AWS:EpochTime': Math.floor(Date.now() / 1000) + ACCESS_DURATION,
        },
      },
    }],
  }
  const policyString = JSON.stringify(policy)
  const policyBase64 = new Buffer(policyString).toString('base64')

  const signature =
    crypto
      .createSign('RSA-SHA1')
      .update(policyString)
      .sign(privateKey, 'base64')

  const normalizeBase64 = string =>
    string.replace(/=/g, '_').replace(/\+/g, '-').replace(/\//g, '~')

  // this is an arbitrary naming convention
  const response = {
    'CloudFront-Policy': normalizeBase64(policyBase64),
    'CloudFront-Signature': normalizeBase64(signature),
    'CloudFront-Key-Pair-Id': CLOUDFRONT_KEY_PAIR_ID,
  }

  callback(null, response)
}
